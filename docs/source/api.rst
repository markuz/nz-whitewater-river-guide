API Reference
=============

Sample code to illustrate ``NZRivers`` code functionality.

.. code-block:: python

    >>> from nzrivers import NZRivers
    >>> r = NZRivers()
    >>> r.load_json()
    >>> for s in r.json:
    ...     print(f"{s['region']} | {s['river']} | {s['section']} | {s['class']}")
    ...     break
    ...     
    ... 
    Tasman | Anatoki | Anatoki Hut to Anatoki Valley sawmill | IV-V | 4.5


NZRivers
--------

.. automodule:: nzrivers
      :members:


NZRiversApp
-----------

.. autoprogram-cliff:: nzrivers.main.NZRiversApp
      :application: New Zealand Whitewater River Guide

ls
--

.. automodule:: nzrivers.ls
      :members:

.. autoprogram-cliff:: nzrivers
      :command: ls

pdf
---

.. automodule:: nzrivers.pdf
      :members:

.. autoprogram-cliff:: nzrivers
      :command: pdf
