# -*- coding: utf-8 -*-
from nzrivers import NZRivers


project = 'New Zealand Whitewater River Guide'
copyright = '2019, Marek Kuziel'
author = 'Marek Kuziel'
version = NZRivers.__version__
release = NZRivers.__version__
extensions = [
    "sphinx.ext.autodoc",
    "cliff.sphinxext",
]
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
language = None
exclude_patterns = []
pygments_style = "bw"
html_static_path = ['_static']
html_title = "NZ Whitewater River Guide Documentation"
html_short_title = "NZ Whitewater River Guide"
html_static_path = ['_static']
import guzzle_sphinx_theme
html_translator_class = 'guzzle_sphinx_theme.HTMLTranslator'
html_theme_path = guzzle_sphinx_theme.html_theme_path()
html_theme = 'guzzle_sphinx_theme'
html_sidebars = {
    '**': ['logo-text.html', 'globaltoc.html', 'searchbox.html']
}
extensions.append("guzzle_sphinx_theme")
html_theme_options = {
    "project_nav_name": "NZ Whitewater River Guide",
    "projectlink": "https://nzwwrg.kuziel.nz",
}
htmlhelp_basename = 'NZ Whitewater River Guide'
html_show_sourcelink = False
html_show_sphinx = True
man_pages = [
    (master_doc, 'nzrivers', 'NZ Whitewater River Guide Documentation',
     [author], 1)
]
