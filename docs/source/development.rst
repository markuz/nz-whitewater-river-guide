Development
===========

To develop ``nzrivers`` via ``pyenv`` + ``virtualenv``, run::

    pyenv virtualenv 3.9.6 nzrivers                                # create new virtualenv
    git clone https://gitlab.com/markuz/nz-whitewater-river-guide  # clone the repo
    cd nz-whitewater-river-guide                                   # get into the repo dir
    pyenv local nzrivers                                           # set nzrivers env as local
    source ~/.pyenv/versions/nzrivers/bin/activate                 # activate new env
    make update                                                    # install dependencies
    make develop                                                   # build development egg
    make docs                                                      # generate documentation
    nzrivers help                                                  # show application help
