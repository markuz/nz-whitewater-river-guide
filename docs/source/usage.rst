Usage
=====

``nzrivers -h`` or ``nzrivers --help`` displays usage page.

Adding ``--debug`` option helps to debug issues during development.

``nzrivers ls --help`` displays help pages for available commands.


``ls``
------

List NZ whitewater river sections data::

    nzrivers ls


Generate CSV file with all data::

    nzrivers ls -f csv --quote=all > nzrivers.csv


List all river sections ordered by their class::


    nzrivers ls -c river -c section -c class --sort-column "class" | less


``pdf``
-------

To generate PDF version of the New Zealand Whitewater River Guide, simply run::

    nzrivers pdf
