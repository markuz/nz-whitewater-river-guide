#!/usr/bin/env python

PROJECT = "nzrivers"

from nzrivers import NZRivers

VERSION = NZRivers.__version__


from setuptools import setup, find_packages

try:
    long_description = open("README.rst", "rt").read()
except IOError:
    long_description = ""

setup(
    name=PROJECT,
    version=VERSION,
    description="New Zealand Whitewater River Guide",
    long_description=long_description,
    author="Marek Kuziel",
    author_email="marek@kuziel.info",
    url="https://gitlab.com/markuz/nz-whitewater-river-guide",
    download_url="https://gitlab.com/markuz/nz-whitewater-river-guide/-/archive/master/nzrivers-master.tar.bz2",
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Intended Audience :: Other Audience",
        "Environment :: Console",
    ],
    platforms=["Any"],
    scripts=[],
    provides=[],
    install_requires=[
        "cliff",
        "jinja2",
        "xhtml2pdf",
    ],
    namespace_packages=[],
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        "console_scripts": ["nzrivers = nzrivers.main:main"],
        "nzrivers": [
            "ls = nzrivers.ls:Ls",
            "pdf = nzrivers.pdf:Pdf",
        ],
    },
    zip_safe=False,
)
