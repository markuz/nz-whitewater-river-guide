NZ Whitewater River Guide
=========================

.. contents::
      :local:


Introduction
------------

NZ Whitewater River Guide is a Python wrapper for https://rivers.org.nz API.

``nzrivers.NZRivers`` provides all core features.

``nzrivers.ls.Ls`` and ``nzrivers.pdf.Pdf`` are 
`cliff – Command Line Interface Formulation Framework <https://docs.openstack.org/cliff/latest/>`_
commands to provide core features via command line.

In essence, the provided commands allow you to create CSV or PDF version of the `New Zealand Whitewater River Guide`.


Installation
------------

Install via ``pip``::

    pip install git+https://gitlab.com/markuz/nz-whitewater-river-guide


Usage
-----

All river section data are stored in ``nzrivers.json`` file.


Generate CSV file::

    nzrivers ls -f csv


Generate PDF file::

    nzrivers pdf


Development
-----------

Git repository is available at https://gitlab.com/markuz/nz-whitewater-river-guide

Pull requests are welcome.


Documentation
-------------

Source of the documentaton is available at the repository https://gitlab.com/markuz/nz-whitewater-river-guide/tree/master/docs/source


Data License
------------

New Zealand Whitewater River Guide licensed per `Attribution-ShareAlike 3.0 New Zealand (CC BY-SA 3.0 NZ) <https://creativecommons.org/licenses/by-sa/3.0/nz/>`_.


Code License
------------

`BSD 3-clause Clear License <https://gitlab.com/markuz/nz-whitewater-river-guide/blob/master/LICENSE>`_
