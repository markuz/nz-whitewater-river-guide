CHANGELOG
=========

2021-08-29 | v0.0.5 - v0.0.9
----------------------------

Further refactoring and clean up.
Migrated the dataset to a new structure.
Updated documentation.


2021-08-28 | v0.0.4
-------------------

Major refactor to use backup version of JSON data instead of the API.
The API was decommissioned sometime during 2020.
The saved dataset contains 322 river sections.
This version is a start of a new era for the dataset.
The idea is to clean it up and add more data to it.
Perhaps even publish as a web application.
But that's future. Let's focus on now.


2019-04-28 | v0.0.3
-------------------

* Added ``pdf`` command


2019-04-27 | v0.0.2
-------------------

* Polished the code
* Added documentation


2019-04-21 | v0.0.1
-------------------

* Initial version
