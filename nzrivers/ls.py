import logging

from cliff.lister import Lister
from nzrivers import NZRivers


class Ls(Lister):

    log = logging.getLogger(__name__)

    nzrivers = NZRivers()

    def take_action(self, parsed_args):
        """Generate list of all NZ whitewater river sections

        This command lists all NZ whitewater river sections data.

        Eg. generate CSV file with all data:

            nzrivers ls -f csv --quote=all > nzrivers.csv


        Eg. list all river sections ordered by their class:

            nzrivers ls -c Section -c River -c Class --sort-column "Class" | less


        The following is a list of all columns returned by this command:

        * section
        * region
        * river
        * class
        * hot_tip
        * character
        * description
        * gradient
        * length
        * level
        * time
        * maps
        * tags
        * put_in
        * take_out
        * shuttle
        * portage
        * history
        * credits
        * created
        * change

        """
        self.nzrivers.load_json()
        data = self.nzrivers.json_to_csv(self.nzrivers.json)
        return ((
            'section',
            'region',
            'river',
            'class',
            'hot_tip',
            'character',
            'description',
            'gradient',
            'length',
            'level',
            'time',
            'maps',
            'tags',
            'put_in',
            'take_out',
            'shuttle',
            'portage',
            'history',
            'credits',
            'created',
            'change',
        ), data)
