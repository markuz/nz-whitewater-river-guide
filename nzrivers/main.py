import sys

from cliff.app import App
from cliff.commandmanager import CommandManager

from nzrivers import NZRivers


class NZRiversApp(App):
    def __init__(self):
        super(NZRiversApp, self).__init__(
            description="New Zealand Whitewater River Guide",
            version=NZRivers.__version__,
            command_manager=CommandManager("nzrivers"),
            deferred_help=True,
        )

    def initialize_app(self, argv):
        self.LOG.debug(f"initialize_app with {argv}")

    def prepare_to_run_command(self, cmd):
        self.LOG.debug(f"prepare_to_run_command {cmd.__class__.__name__}")

    def clean_up(self, cmd, result, err):
        self.LOG.debug(f"clean_up {cmd.__class__.__name__}")
        if err:
            self.LOG.debug(f"got an error: {err}")


def main(argv=sys.argv[1:]):
    myapp = NZRiversApp()
    return myapp.run(argv)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
