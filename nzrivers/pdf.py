import os
import jinja2
import logging

from xhtml2pdf import pisa
from cliff.command import Command
from nzrivers import NZRivers
from jinja2 import Template
from jinja2.exceptions import TemplateNotFound


class Pdf(Command):
    """Generate New Zealand Whitewater River Guide in PDF format
    """

    log = logging.getLogger(__name__)

    nzrivers = NZRivers()

    __template__ = """
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="New Zealand Whitewater River Guide">
  <header>
    <title>New Zealand Whitewater River Guide</title>
    <style>
    @font-face {
      font-family: 'PT_Sans-Web-Bold';
      font-weight: bold;
      font-style: italic;
      src: url("fonts/PT_Sans/PT_Sans-Web-Bold.ttf");
    }
    @font-face {
      font-family: 'PT_Sans-Web-Italic';
      font-style: italic;
      src: url("fonts/PT_Sans/PT_Sans-Web-Italic.ttf");
    }
    @font-face {
      font-family: 'PT_Sans-Web-Regular';
      src: url("fonts/PT_Sans/PT_Sans-Web-Regular.ttf");
    }
    @font-face {
      font-family: 'Merriweather';
      src: url("fonts/Merriweather/Merriweather-Black.ttf");
    }
    body {
      font-family: 'PT_Sans-Web-Regular', sans-serif;
      font-size: 20px;
      margin: 0;
      padding: 0;
    }
    h1 {
      font-family: 'Merriweather', serif;
      font-size: 1.9em;
      margin: 0;
      padding: 460px 0;
    }
    h2 {
      font-family: 'Merriweather', serif;
      font-size: 1.75em;
      margin: 0;
      padding: 460px 0 475px;
    }
    .small {
      font-size: 0.77em;
    }
    h3 {
      font-family: 'Merriweather', serif;
      font-size: 1.5em;
      margin: 0;
      padding: 460px 0 460px;
    }
    h4 {
      font-family: 'Merriweather', serif;
      font-size: 1.25em;
      margin: 0;
      padding: 50px 0 25px;
    }
    h5 {
      font-family: 'Merriweather', serif;
      font-size: 1.1em;
      margin: 0;
      padding: 25px 0 10px;
    }
    h6 {
      font-family: 'Merriweather', serif;
    }
    blockquote {
      font-family: 'PT_Sans-Web-Italic', sans-serif;
      font-size: 1.1em;
      font-style: italic;
      padding-top: 15px;
      padding-bottom: 30px;
    }
    .section {
      padding-top: 30px;
    }
    table {
      margin: 0;
      padding: 0 0 15px;
    }
    th {
      font-family: 'PT_Sans-Web-Bold', sans-serif;
      width: 200px;
      text-align: left;
      margin: 0;
      padding: 0 2px 2px 0;
    }
    td {
      margin: 0;
      padding: 0 2px 2px 0;
    }
    .put-in {
    }
    .take-out {
    }
    .description {
    }
    .history {
    }
    .credits {
      font-size: 0.9em;
    }
    .tag {
      float: left;
    }
    .clear {
      display: block;
      border-top: 1px solid #888;
      margin: 20px 0 0;
      padding: 0;
    }
    </style>
  </header>
  <body>

    <h1>New Zealand Whitewater River Guide</h1>

    <h2><a href="{{ nzrivers.url }}">{{ nzrivers.licence_attribution }}</a></h2>

    <h2>This document was created using <a class="small" href="https://gitlab.com/markuz/nz-whitewater-river-guide">gitlab.com/markuz/nz-whitewater-river-guide</a></h2>

    {% for region, rivers in nzrivers.guide.items() %}

    <h2>{{ region }}</h2>

      {% for river, sections in rivers.items() %}

    <h3>{{ river }}</h3>

        {% for name, section in sections.items() %}

    <h4>{{ name }}</h4>

    <div class="section">

      <h5>Information</h5>

      {% if section.hot_tip %}
      <blockquote>{{ section.hot_tip }}</blockquote>
      {% endif %}

      {% if section.credits %}
      <div class="credits">Credits: <b>{{ section.credits }}</b></div>
      {% endif %}

      {% if section.tags %}<div class="tags">Tags: {% for tag in section.tags %}{{ tag }}{% endfor %}</div>{% endif %}

      <table>

        {% if section.character %}
        <tr>
          <th>Gradient</th>
          <td>{{ section.gradient }}{{ section.gradient_unit }}</td>
        </tr>
        {% endif %}

        {% if section.class %}
        <tr>
          <th>Class</th>
          <td>{{ section.class }}</td>
        </tr>
        {% endif %}

        {% if section.length != "0" %}
        <tr>
          <th>Length</th>
          <td>{{ section.length }}{{ section.length_unit }}</td>
        </tr>
        {% endif %}

        {% if section.level %}
        <tr>
          <th>Level</th>
          <td>{{ section.level }}</td>
        </tr>
        {% endif %}

        {% if section.shuttle %}
        <tr>
          <th>Shuttle</th>
          <td>{{ section.shuttle }}</td>
        </tr>
        {% endif %}

        {% if section.time %}
        <tr>
          <th>Time</th>
          <td>{{ section.time }}</td>
        </tr>
        {% endif %}

        {% if section.maps %}
        <tr>
          <th>Maps</th>
          <td>{{ section.maps }}</td>
        </tr>
        {% endif %}

        <tr>
          <th>Created</th>
          <td>{{ section.created[0:10] }}</td>
        </tr>

        <tr>
          <th>Last Change</th>
          <td>{{ section.changed[0:10] }}</td>
        </tr>

      </table>

      {% if section.put_in %}
      <h5>Put In</h5>
      <div class="putin">{{ section.put_in }}</div>
      {% endif %}

      {% if section.take_out %}
      <h5>Take Out</h5>
      <div class="takeout">{{ section.take_out }}</div>
      {% endif %}

      {% if section.description %}
      <h5>Description</h5>
      <div class="description">{{ section.description|replace("<h2>", "<h6>")|replace("</h2>", "</h6>") }}</div>
      {% endif %}

      {% if section.history %}
      <h5>History</h5>
      <div class="history">{{ section.history }}</div>
      {% endif %}

    </div>
    <div class="clear">&nbsp;</div>

        {% endfor %}

      {% endfor %}

    {% endfor %}

  </body>
</html>
        """

    def take_action(self, parsed_args):
        """Generates report as a PDF file.
        """
        self.nzrivers.load_json()
        self.nzrivers.guide = self.nzrivers.get_guide(self.nzrivers.json)

        in_html = self.generate_html()

        out_pdf = "nzrivers.pdf"
        if parsed_args.file:
            out_pdf = parsed_args.file

        self.generate_pdf(in_html, out_pdf)

    def get_description(self):
        """creates PDF report"""
        return "creates PDF report"

    def get_parser(self, prog_name):
        """Defines the following input arguments for ``pdf`` command:

        * ``-f`` ``--file`` destination of the pdf file
        """
        parser = super(Pdf, self).get_parser(prog_name)
        parser.add_argument("-f", "--file")
        return parser

    def generate_html(self):
        """Generates HTML version"""
        try:
            loader = jinja2.FileSystemLoader(searchpath=[os.getcwd()])
            env = jinja2.Environment(loader=loader)
            template = env.get_template("nzrivers.html")
            self.log.debug(f"Template: {self.nzrivers.pdf_template_file}")
            return template.render(nzrivers=self.nzrivers)
        except TemplateNotFound:
            template = Template(self.__template__)
            self.log.info("Template: internal")
            return template.render(nzrivers=self.nzrivers)

    def generate_pdf(self, in_html, out_pdf):
        """Generates PDF from HTML version"""
        report_file = open(out_pdf, "w+b")
        status = pisa.CreatePDF(
            in_html.encode('utf-8'),
            dest=report_file,
            encoding='utf-8')
        report_file.close()
