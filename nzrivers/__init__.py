# -*- coding: utf-8 -*-

"""New Zealand Whitewater River Guide"""

import os
import csv
import json

from datetime import datetime


class NZRivers(object):
    """Notes:

    * gradient unit is m/km
    * length unit is km
    """

    __version__ = "0.0.9"

    json = []

    csv = []

    licence_attribution = "Attribution-ShareAlike 3.0 New Zealand (CC BY-SA 3.0 NZ)"

    licence_url = "https://creativecommons.org/licenses/by-sa/3.0/nz/"

    structure = [
        'section',
        'region',
        'river',
        'class',
        'description',
        'character',
        'gradient',
        'length',
        'level',
        'time',
        'maps',
        'tags',
        'put_in',
        'take_out',
        'hot_tip',
        'shuttle',
        'portage',
        'history',
        'credits',
        'created',
        'changed',
    ]

    def load_json(self, filename="nzrivers.json"):
        """Load JSON data.

        :param filename: name of the file to load; default ``nzrivers.json``
        :type filename: string
        """
        with open(filename, "r") as data_file:
            self.json = json.load(data_file)

    def save_json(self, filename="nzrivers.json"):
        """Save JSON data.

        :param filename: name of the file to save; default ``nzrivers.json``
        :type filename: string
        """
        with open(filename, "w") as data_file:
            json.dump(self.json, data_file)

    def load_csv(self, filename="nzrivers.csv"):
        """Load CSV data.

        :param filename: name of the file to load; default ``nzrivers.csv``
        :type filename: string
        """
        with open("nzrivers.csv", newline="") as data_file:
            rivers = csv.reader(data_file, delimiter=",", quotechar='"')
            for river in rivers:
                # skip reading header
                if river[0] == "section" and river[-1] == "changed":
                    continue
                self.csv.append(tuple(river))

    def save_csv(self, filename="nzrivers.csv"):
        """Save CSV data.

        :param filename: name of the file to save; default ``nzrivers.csv``
        :type filename: string
        """
        with open(filename, "w", newline="") as data_file:
            rivers = csv.writer(data_file, delimiter=",", quotechar='"')
            # save header first
            rivers.writerow(self.structure)
            for river in self.csv:
                rivers.writerow(list(river))

    def json_to_csv(self, json_data):
        """Converts JSON to CSV.

        :param json_data: list with all section dict data
        :type json_data: list
        :return: ``sections`` - all processed river sections
        :rtype: list
        """
        sections = []
        change = datetime.now().astimezone().replace(microsecond=0).isoformat()
        for section in json_data:
            sections.append((
                section.get('section', None),
                section.get('region', None),
                section.get('river', None),
                section.get('class', None),
                section.get('hot_tip', None),
                section.get('character', None),
                section.get('description', None),
                section.get('gradient', None),
                section.get('length', None),
                section.get('level', None),
                section.get('time', None),
                section.get('maps', None),
                section.get('tags', None),
                section.get('put_in', None),
                section.get('take_out', None),
                section.get('shuttle', None),
                section.get('portage', None),
                section.get('history', None),
                section.get('credits', None),
                section.get('created', None),
                change,
            ))
        return sections

    def csv_to_json(self, csv_data):
        """Converts CSV to JSON.

        :param csv_data: list with all section tuple data
        :type csv_data: list
        :return: ``sections`` - all processed river sections
        :rtype: list
        """
        change = datetime.now().astimezone().replace(microsecond=0).isoformat()
        sections = []
        for section in csv_data:
            sections.append({
                'section': section[0],
                'region': section[1],
                'river': section[2],
                'class': section[3],
                'hot_tip': section[4],
                'character': section[5],
                'description': section[6],
                'gradient': section[7],
                'length': section[8],
                'level': section[9],
                'time': section[10],
                'maps': section[11],
                'tags': section[12],
                'put_in': section[13],
                'take_out': section[14],
                'shuttle': section[15],
                'portage': section[16],
                'history': section[17],
                'credits': section[18],
                'created': section[19],
                'changed': change,
            })
        return sections

    def get_guide(self, sections):
        """Sort given sections JSON data into guide format:

            Regions -> Rivers -> Sections

        :param sections: list with all section dict data
        :type sections: list
        :return: ``guide`` - processed river guide
        :rtype: dict
        """
        pass1 = dict()
        for s in sections:
            if s["region"] not in pass1:
                pass1[s["region"]] = dict()
            if s["river"] not in pass1[s["region"]]:
                pass1[s["region"]][s["river"]] = dict()
            pass1[s["region"]][s["river"]][s["section"]] = s
        guide = dict()
        for region in sorted(pass1):
            guide[region] = dict()
            for river in sorted(pass1[region]):
                guide[region][river] = dict()
                for section in sorted(pass1[region][river]):
                    guide[region][river][section] = pass1[region][river][section]  # NOQA
        return guide
